import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Avatar, ListItem } from "react-native-elements";
import { useNavigation } from "@react-navigation/native";
import { db } from "../Firebase";

const Item = ({ id, chatname, enterchat }) => {
  const navigation = useNavigation();
  const [lastmessage, setLastmessage] = useState([]);

  function nav(id, chatname) {
    navigation.navigate("Chat", { id, chatname });
  }

  useEffect(() => {
    const uns = db
      .collection("Chat")
      .doc(id)
      .collection("messages")
      .orderBy("timestamp", "desc")
      .onSnapshot((snapshot) =>
        setLastmessage(snapshot.docs.map((doc) => doc.data()))
      );
    return uns;
  });

  return (
    <ListItem
      onPress={() => {
        // enterchat(id, chatname);
        nav(id, chatname);
      }}
      key={id}
      bottomDivider
    >
      <Avatar
        rounded
        source={{
          uri: "https://www.clipartmax.com/png/middle/132-1323860_how-to-use-this-website-e-learning-student-icon.png",
        }}
      />
      <ListItem.Content>
        <ListItem.Title>{chatname}</ListItem.Title>
        <ListItem.Subtitle numberOfLines={1}>
          {/* {lastmessage?.[0]?.displayName}: */}
          {lastmessage?.[0]?.message}
        </ListItem.Subtitle>
      </ListItem.Content>
    </ListItem>
  );
};

export default Item;

const styles = StyleSheet.create({});
