import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { Input, Button, Text } from "react-native-elements";
import { auth } from "../Firebase";
const SignupScreens = ({ navigation }) => {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  function register() {
    auth.createUserWithEmailAndPassword(email,password).then(authUser=>{
        authUser.user.updateProfile({
            displayName:username,
        })
    }).catch(err=>alert(err))
  }

  return (
    <View style={styles.container}>
      <Text
        style={{
          width: "100%",
          paddingLeft: 20,
          paddingTop: 20,
          color: "#7f8c8d",
        }}
        h4
      >
        Create Account
      </Text>
      <View>
        <View style={styles.input}>
          <Input
            leftIcon={{
              type: "font-awesome",
              name: "user",
              style: { marginRight: 10 },
              color: "#7f8c8d",
              size: 20,
            }}
            placeholder="User Name"
            // autoFocus
            value={username}
            onChangeText={(val) => setUsername(val)}
          />
          <Input
            leftIcon={{
              type: "font-awesome",
              name: "envelope",
              style: { marginRight: 10 },
              color: "#7f8c8d",
              size: 20,
            }}
            placeholder="Email"
            type="email"
            value={email}
            onChangeText={(val) => setEmail(val)}
          />
          <Input
            leftIcon={{
              type: "font-awesome",
              name: "key",
              style: { marginRight: 10 },
              color: "#7f8c8d",
              size: 20,
            }}
            placeholder="Password"
            secureTextEntry
            type="password"
            value={password}
            onChangeText={(val) => setPassword(val)}
          />
        </View>
        <Button
          onPress={register}
          containerStyle={styles.button}
          title="Signup"
        />
      </View>
    </View>
  );
};

export default SignupScreens;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-between",
  },
  input: {
    width: 300,
  },
  button: {
    marginTop: 10,
    width: 300,
    marginBottom: 40,
  },
});
