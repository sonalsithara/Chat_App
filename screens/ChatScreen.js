import React, { useLayoutEffect, useState, useRef } from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  SafeAreaView,
} from "react-native";
import { useNavigation, useRoute } from "@react-navigation/native";
import { Icon } from "react-native-elements";
import { StatusBar } from "expo-status-bar";
import { KeyboardAvoidingView } from "react-native";
import { ScrollView } from "react-native";
import { TextInput } from "react-native";
import { Keyboard } from "react-native";
import { TouchableWithoutFeedback } from "react-native";
import * as firebase from "firebase";
import { db, auth } from "../Firebase";

const ChatScreen = () => {
  const route = useRoute();
  const navigation = useNavigation();
  const [message, setMessage] = useState("");
  const [data, setData] = useState([]);
  const scrollViewRef = useRef();

  useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: () => (
        <View>
          <Text style={{ color: "white", fontSize: 18 }}>
            {route.params.chatname}
          </Text>
        </View>
      ),
      headerRight: () => (
        <View
          style={{
            flexDirection: "row",
            width: 100,
            justifyContent: "space-around",
          }}
        >
          <TouchableOpacity>
            <Icon size={20} name="videocamera" type="antdesign" color="white" />
          </TouchableOpacity>
          <TouchableOpacity>
            <Icon size={20} name="phone" type="antdesign" color="white" />
          </TouchableOpacity>
        </View>
      ),
    });
  }, [navigation]);

  const sendMessage = () => {
    Keyboard.dismiss();
    db.collection("Chat").doc(route.params.id).collection("messages").add({
      timestamp: firebase.default.firestore.FieldValue.serverTimestamp(),
      message: message,
      displayName: auth.currentUser.displayName,
      email: auth.currentUser.email,
    });
    setMessage("");
  };

  useLayoutEffect(() => {
    const uns = db
      .collection("Chat")
      .doc(route.params.id)
      .collection("messages")
      .orderBy("timestamp", "asc")
      .onSnapshot((snapshot) =>
        setData(
          snapshot.docs.map((doc) => ({
            id: doc.id,
            data: doc.data(),
          }))
        )
      );
    return uns;
  }, [route]);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <StatusBar style="light" />
      <KeyboardAvoidingView
        behavior="height"
        style={styles.container}
        keyboardVerticalOffset={90}
      >
        {/* <TouchableWithoutFeedback onPress={Keyboard.dismiss()}> */}
        <>
          <ScrollView
            contentContainerStyle={{ paddingTop: 15 }}
            ref={scrollViewRef}
            onContentSizeChange={() =>
              scrollViewRef.current.scrollToEnd({ animated: true })
            }
          >
            {data.map((val) =>
              val.data.email === auth.currentUser.email ? (
                <View key={val.id} style={styles.reciver}>
                  <Text style={styles.recivertext}>{val.data.message}</Text>
                </View>
              ) : (
                <View key={val.id} style={styles.sender}>
                  <Text style={styles.sendertext}>{val.data.message}</Text>
                </View>
              )
            )}
          </ScrollView>
          <View style={styles.footer}>
            <TextInput
              value={message}
              onChangeText={(val) => setMessage(val)}
              style={styles.textinput}
              placeholder="Message"
            />
            <TouchableOpacity onPress={sendMessage}>
              <Icon size={36} name="send" type="ionicons" color="#3498db" />
            </TouchableOpacity>
          </View>
        </>
        {/* </TouchableWithoutFeedback> */}
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default ChatScreen;

const styles = StyleSheet.create({
  container: { flex: 1 },
  footer: {
    flexDirection: "row",
    alignItems: "center",
    width: "100%",
    padding: 15,
  },
  reciver: {
    padding: 12,
    backgroundColor: "rgba(189, 195, 199, 0.4)",
    alignSelf: "flex-end",
    borderRadius: 20,
    marginRight: 12,
    marginBottom: 20,
    maxWidth: "80%",
    position: "relative",
  },
  sender: {
    padding: 12,
    backgroundColor: "rgba(52, 152, 219,1)",
    alignSelf: "flex-start",
    borderRadius: 20,
    marginLeft: 12,
    marginBottom: 20,
    maxWidth: "80%",
    position: "relative",
  },
  sendertext: { color: "white" },
  textinput: {
    bottom: 0,
    height: 40,
    flex: 1,
    marginRight: 15,
    borderColor: "#3498db",
    borderWidth: 1,
    padding: 10,
    color: "grey",
    borderRadius: 30,
  },
});
