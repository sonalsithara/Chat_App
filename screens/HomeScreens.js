import React, { useLayoutEffect, useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  SafeAreaView,
  ScrollView,
  View,
  TouchableOpacity,
} from "react-native";
import Item from "../component/Item";
import { StatusBar } from "expo-status-bar";
import { Icon } from "react-native-elements";
import { auth, db } from "../Firebase";

const HomeScreens = ({ navigation }) => {
  const [chats, setChats] = useState([]);

  useEffect(() => {
    const uns = db.collection("Chat").onSnapshot((snapshot) => {
      setChats(
        snapshot.docs.map((doc) => ({
          id: doc.id,
          data: doc.data(),
        }))
      );
    });
    return uns;
  }, []);

  function signout() {
    auth.signOut().then(() => {
      navigation.replace("Login");
    });
  }

  function camera() {}

  useLayoutEffect(() => {
    navigation.setOptions({
      title: "Volume Max",
      headerLeft: () => (
        <View style={{ paddingLeft: 26 }}>
          <TouchableOpacity onPress={signout}>
            <Icon size={20} name="logout" type="antdesign" color="white" />
          </TouchableOpacity>
        </View>
      ),
      headerRight: () => (
        <View
          style={{
            flexDirection: "row",
            width: 100,
            justifyContent: "space-around",
          }}
        >
          <TouchableOpacity onPress={camera}>
            <Icon size={20} name="camerao" type="antdesign" color="white" />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              navigation.navigate("AddChat");
            }}
          >
            <Icon size={20} name="pencil" type="font-awesome" color="white" />
          </TouchableOpacity>
        </View>
      ),
    });
  }, [navigation]);

  // const enterchat = (id, chatname) => {
  //   navigation.navigate("Chat", { id, chatname });
  // };

  return (
    <SafeAreaView>
      <StatusBar style="light" />
      <ScrollView style={styles.container}>
        {chats.map((val) => (
          <Item
            key={val.id}
            id={val.id}
            chatname={val.data.chatName}
            // enterchat={enterchat}
          />
        ))}
      </ScrollView>
    </SafeAreaView>
  );
};

export default HomeScreens;

const styles = StyleSheet.create({
  container: {
    height: "100%",
  },
});
