import React, { useLayoutEffect, useState } from "react";
import { StyleSheet, Text, View, KeyboardAvoidingView } from "react-native";
import { Input, Button } from "react-native-elements";
import { db } from "../Firebase";
const AddChatScreen = ({ navigation }) => {
  const [chatname, setchatname] = useState("");

  useLayoutEffect(() => {
    navigation.setOptions({
      title: "Add New Chat",
    });
  }, [navigation]);

  async function addChat(params) {
    await db
      .collection("Chat")
      .add({
        chatName: chatname,
      })
      .then(() => navigation.goBack())
      .catch((err) => alert(err));
  }

  return (
    <KeyboardAvoidingView
      behavior="padding"
      enabled={false}
      style={styles.container}
    >
      <View style={styles.main}>
        <Input
          leftIcon={{
            type: "antdesign",
            name: "wechat",
            style: { marginRight: 10 },
            color: "#7f8c8d",
            size: 20,
          }}
          placeholder="Chat Name"
          value={chatname}
          onChangeText={(val) => setchatname(val)}
        />
        <Button title="Add Chat" onPress={addChat} />
      </View>
    </KeyboardAvoidingView>
  );
};

export default AddChatScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  main: {
    width: 300,
  },
});
