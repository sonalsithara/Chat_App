import { StatusBar } from "expo-status-bar";
import React, { useState, useEffect } from "react";
import { StyleSheet, View } from "react-native";
import { Input, Button, Image } from "react-native-elements";
import { auth } from "../Firebase";

const LoginScreen = ({ navigation }) => {
  const [email, setemail] = useState("");
  const [password, setpassword] = useState("");

  useEffect(() => {
    const uns = auth.onAuthStateChanged((authUser) => {
      if (authUser) {
        navigation.replace("Home");
      }
    });
    return uns;
  }, []);

  function login() {
      auth.signInWithEmailAndPassword(email,password).catch((err)=>alert(err))
  }

  return (
    <View style={styles.container}>
      <StatusBar style="light" />
      <View style={styles.imagewrapper}>
        <Image
          source={require("../assets/chat.png")}
          style={{
            width: 90,
            height: 90,
          }}
        />
      </View>
      <View>
        <View style={styles.input}>
          <Input
            leftIcon={{
              type: "font-awesome",
              name: "envelope",
              style: { marginRight: 10 },
              color: "#7f8c8d",
              size: 20,
            }}
            placeholder="Email"
            autoFocus
            type="email"
            value={email}
            onChangeText={(val) => {
              setemail(val);
            }}
          />
          <Input
            leftIcon={{
              type: "font-awesome",
              name: "key",
              style: { marginRight: 10 },
              color: "#7f8c8d",
              size: 20,
            }}
            placeholder="Password"
            secureTextEntry
            type="password"
            value={password}
            onChangeText={(val) => {
              setpassword(val);
            }}
          />
        </View>
        <View>
          <Button
            onPress={login}
            containerStyle={styles.button}
            title="Login"
          />
          <Button
            containerStyle={styles.button}
            onPress={() => navigation.navigate("Signup")}
            title="Singup"
            type="outline"
          />
        </View>
      </View>
    </View>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "transparent",
    paddingBottom: 40,
  },
  input: {
    width: 300,
  },
  button: {
    marginTop: 10,
    width: 300,
  },
  imagewrapper: {
    flex: 1,
    justifyContent: "center",
  },
});
