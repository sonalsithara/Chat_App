import * as fire from "firebase";
import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyBNjEVZHVofFzwNXIErtTIHAsT0EYWV5S0",
  authDomain: "chatapp-23520.firebaseapp.com",
  projectId: "chatapp-23520",
  storageBucket: "chatapp-23520.appspot.com",
  messagingSenderId: "322555274780",
  appId: "1:322555274780:web:e7291202180c7399b5e1d9",
};

let app;

if (fire.default.apps.length === 0) {
  app = fire.default.initializeApp(firebaseConfig);
} else {
  app = fire.default.app();
}

const auth = fire.default.auth();
const db = fire.default.firestore();

export { auth, db };
